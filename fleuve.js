'use strct';

var $fleutext;
var $fleuview;

$(document).ready(function() {
  
  $fleutext = $('.fleutext');
  $fleuview = $('.fleuview');

  $fleutext.on('input propertychange', function() {
    $fleuview.html(fleuParse($fleutext.val()));
    $fleuview.find('pre code').each(function(i, block) {
      window.testblock = block;
      var contents = block.innerHTML;
      contents = contents.replace(/<br>/g, "\n");
      block.innerHTML = contents;
      hljs.highlightBlock(block);
    });
  });

});

function fleuParse(str) {
  str = escapeHtml(str);

  var builder = [];
  var bold = false;
  var wrappers = [BoldWrapper, ItalicWrapper, ColorWrapper, BrWrapper, CodeWrapper, BulletWrapper, DBulletWrapper, TitleWrapper];
  var openWrappers = [];

  var i = 0;
  parser:
    while(i < str.length) {

      // at the end of every line, call openWrappers
      var j = openWrappers.length;
      if(str.charAt(i) == "\n") {
        while(j--) {
          var wrapper = openWrappers[j];
          if(wrapper.closed) {
            openWrappers.splice(j, 1);
            continue;
          }
          if(wrapper.lineEndCallback) {
            builder.push(wrapper.lineEndCallback());
          }
        }
      }

      // check openWrappers first
      var j = openWrappers.length;
      while(j--) {
        var wrapper = openWrappers[j];
        if(wrapper.closed) {
          openWrappers.splice(j, 1);
          continue;
        }
        if(wrapper.canClaim(str, i)){
          var claim = wrapper.claim(str, i);
          if(claim) {
            builder.push(claim.callback.bind(wrapper));
            i += claim.size;
            continue parser;
          }
        }
      }

      // then check wrapper prototypes
      var j = wrappers.length
        while(j--) {
          var wrapper = wrappers[j];
          if(wrapper.prototype.canClaim(str, i)){
            var w = new wrapper();
            var claim = w.claim(str, i);
            if(claim) {
              builder.push(claim.callback.bind(w));
              i += claim.size;
              if(!w.closed) {
                openWrappers.push(w);
              }
              continue parser;
            }
          }
        }

      // if not claimed, add the raw character.
      builder.push(str.charAt(i));
      i++;
    }

  // check any unclosed wrappers
  var k = openWrappers.length;
  while(k--) {
    var wrapper = openWrappers[k];
    if(wrapper.closed) {
      continue;
    }
    if(wrapper.unclosedCallback) {
      builder.push(wrapper.unclosedCallback());
    }
  }

  console.log(builder);

  // build it!
  var parsed = "";
  for(var i = 0; i < builder.length; i++) {
    var item = builder[i];
    parsed += (typeof(item) === "string") ? item : item();
  }
  return parsed;
}

function TitleWrapper() {
  this.closed = false;
}

TitleWrapper.prototype.canClaim = function(str, ind) {
  if(ind === 0) {
    if(/&#x3D;&#x3D;&#x3D;/.test(str.slice(ind, ind+18)) === true) {
      return true;
    }
    return false;
  }
  else if(/\n&#x3D;&#x3D;&#x3D;/.test(str.slice(ind-1, ind+18)) === true) {
    return true;
  }
  return false;
}

TitleWrapper.prototype.claim = function(str, ind) {
  return { size: 18, callback: this.openCallback };
}

TitleWrapper.prototype.openCallback = function(str, ind) {
  return "<span class='fleu-title'>";
}

TitleWrapper.prototype.lineEndCallback = function(str, ind) {
  this.closed = true;
  return "</span>";
}


function BulletWrapper() {
  this.closed = false;
}

BulletWrapper.prototype.canClaim = function(str, ind) {
  if(ind === 0) {
    if(/- /.test(str.slice(ind, ind+2)) === true) {
      return true;
    }
    return false;
  }
  if(/\n- /.test(str.slice(ind-1, ind+2)) === true) {
    return true;
  }
  return false;
}

BulletWrapper.prototype.claim = function(str, ind) {
  return { size: 2, callback: this.openCallback };
}

BulletWrapper.prototype.openCallback = function(str, ind) {
  return "<span class='fleu-bullet'>";
}

BulletWrapper.prototype.lineEndCallback = function(str, ind) {
  this.closed = true;
  return "</span>";
}

function DBulletWrapper() {
  this.closed = false;
}

DBulletWrapper.prototype.canClaim = function(str, ind) {
  if(ind === 0) {
    if(/-- /.test(str.slice(ind, ind+3)) === true) {
      return true;
    }
    return false;
  }
  if(/\n-- /.test(str.slice(ind-1, ind+3)) === true) {
    return true;
  }
  return false;
}

DBulletWrapper.prototype.claim = function(str, ind) {
  return { size: 2, callback: this.openCallback };
}

DBulletWrapper.prototype.openCallback = function(str, ind) {
  return "<span class='fleu-doublebullet'>";
}

DBulletWrapper.prototype.lineEndCallback = function(str, ind) {
  this.closed = true;
  return "</span>";
}

function BrWrapper() {
  this.closed = true;
}

BrWrapper.prototype.canClaim = function(str, ind) {
  if(str.charAt(ind) === "\n") {
    return true;
  }
  return false;
}

BrWrapper.prototype.claim = function(str, ind) {
  return { size: 1, callback: this.brCallback };
}

BrWrapper.prototype.brCallback = function(str, ind) {
  return "<br />";
}

function CodeWrapper() {
  this.opened = false;
  this.closed = false;
  this.lang = '';
}

CodeWrapper.prototype.canClaim = function(str, ind) {
  if(str.slice(ind, ind+18) === "&#x60;&#x60;&#x60;") {
    return true;
  }
  return false;
}

CodeWrapper.prototype.findLanguage = function(str, ind) {
  if(str.slice(ind, ind+18) === "&#x60;&#x60;&#x60;") {
    var eol = str.indexOf("\n", ind);
    if(eol > -1) {
      return str.slice(ind+18, eol);
    }
  }
  return false;
}

CodeWrapper.prototype.claim = function(str, ind) {
  if(this.opened === false) {
    this.lang = this.findLanguage(str, ind); 
    this.opened = true;
    return { size: 19 + this.lang.length, callback: this.openCallback };
  }
  else if(this.closed === false) {
    this.closed = true;
    var newline = (str.charAt(ind + 18) === '\n') ? 1 : 0;
    return { size: 18 + newline, callback: this.closeCallback };
  }
}

CodeWrapper.prototype.openCallback = function(str, ind) {
  if(this.closed) {
    if(this.lang) {
      return "<pre><code class='" + this.lang + "'>";
    }
    return "<pre><code>";
  }
  else {
    if(this.lang) {
      return "&#x60;&#x60;&#x60;" + this.lang
    }
    return "&#x60;&#x60;&#x60;<br>"
  }
}

CodeWrapper.prototype.closeCallback = function(str, ind) {
  return "</code></pre>";
}


function BoldWrapper() {
  this.opened = false;
  this.closed = false;
}

BoldWrapper.prototype.canClaim = function(str, ind) {
  if(str.slice(ind, ind+2) === "**") {
    return true;
  }
  return false;
}

BoldWrapper.prototype.claim = function(str, ind) {
  if(this.opened === false) {
    this.opened = true;
    return { size: 2, callback: this.openCallback };
  }
  else if(this.closed === false) {
    this.closed = true;
    return { size: 2, callback: this.closeCallback };
  }
}

BoldWrapper.prototype.openCallback = function(str, ind) {
  return (this.closed) ? "<b>" : "**";
}

BoldWrapper.prototype.closeCallback = function(str, ind) {
  return "</b>";
}


function ItalicWrapper() {
  this.opened = false;
  this.closed = false;
}

ItalicWrapper.prototype.canClaim = function(str, ind) {
  if(str.slice(ind, ind+2) === "__") {
    return true;
  }
}

ItalicWrapper.prototype.claim = function(str, ind) {
  if(this.opened == false) {
    this.opened = true;
    return { size: 2, callback: this.openCallback };
  }
  else if(this.closed == false) {
    this.closed = true;
    return { size: 2, callback: this.closeCallback };
  }
}

ItalicWrapper.prototype.openCallback = function(str, ind) {
  return (this.closed) ? "<i>" : "__";
}

ItalicWrapper.prototype.closeCallback = function(str, ind) {
  return "</i>";
}


function ColorWrapper() {
  this.opened = false;
  this.closed = false;
}

ColorWrapper.prototype.canClaim = function(str, ind) {
  var color = this.validate(str, ind);
  if(color.length === 3 || color.length === 6) {
    return true;
  }
  return false;
}

ColorWrapper.prototype.validate = function(str, ind) {
  if(str.charAt(ind) !== "#") {
    return "";
  }

  var color = "";
  if(str.charAt(ind+4) === "#"){
    color = str.slice(ind+1, ind+4);
  }
  else if(str.charAt(ind+7) === "#"){
    color = str.slice(ind+1, ind+7);
  }
  var i = color.length;
  while(i--) {
    if("abcdefABCDEF0123456789".indexOf(color.charAt(i)) < 0) {
      return "";
    }
  }

  return color;
}

ColorWrapper.prototype.claim = function(str, ind) {
  var color = this.validate(str, ind);
  if(color.length !== 3 && color.length !== 6) {
    return;
  }
  if(this.opened == false) {
    this.opened = true;
    return { size: 2 + color.length, callback: this.firstCallback.bind({color: color}) };
  }
  return { size: 2 + color.length, callback: this.subsequentCallback.bind({color: color}) };
}

ColorWrapper.prototype.firstCallback = function(str, ind) {
  return "<span style='color: #" + this.color + ";'>";
}

ColorWrapper.prototype.subsequentCallback = function(str, ind) {
  return "</span><span style='color: #" + this.color + ";'>";
}

ColorWrapper.prototype.unclosedCallback = function() {
  return "</span>";
}

var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}

